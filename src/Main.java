/**
 * According to Livinus, not the optimal solution, but it works.
 */
public class Main {
    public static void main (String[] args)
    {
        textToNum("123-647-EYES");
        textToNum("(325)444-TEST");
        textToNum("653-TRY-THIS");
        textToNum("435-224-7613");
    }

    /**
     * Changes every character according to the table.
     * @param input
     */
    public static void textToNum(String input) {
        input = input.replaceAll("[A-C]", "2")
                     .replaceAll("[D-F]", "3")
                     .replaceAll("[G-I]", "4")
                     .replaceAll("[J-L]", "5")
                     .replaceAll("[M-O]", "6")
                     .replaceAll("[P-S]", "7")
                     .replaceAll("[T-V]", "8")
                     .replaceAll("[W-Z]", "9");

        System.out.println(input);

        // The commented lines shows an alternative method.
        /*String s1  = input.replaceAll("[A-C]", "2");
        String s2  = s1.replaceAll("[D-F]", "3");
        String s3  = s2.replaceAll("[G-I]", "4");
        String s4  = s3.replaceAll("[J-L]", "5");
        String s5  = s4.replaceAll("[M-O]", "6");
        String s6  = s5.replaceAll("[P-S]", "7");
        String s7  = s6.replaceAll("[T-V]", "8");
        String s8  = s7.replaceAll("[W-Z]", "9");*/

        //System.out.println(s8);
    }
}